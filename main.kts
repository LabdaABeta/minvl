#!/usr/bin/env kscript

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOnMaven("no.tornado:tornadofx:1.7.20")

@file:Include("vl.kt")

import tornadofx.*

fun main() {
    var state = VL()

    while (state != "") {
        val actions = VL(state).split(",")

        val selected = actions.random()
        println(r(state))
        println("Press ENTER to apply $selected...")
        readLine()

        state = VL(state, selected)
    }
}
