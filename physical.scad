$fa = 1;
$fs = 0.05;

tolerance = 0.001;

GEM_RADIUS = 0.3;
GEM_HEIGHT = 0.4;
BOARD_THICKNESS = 0.1;
TILE_HEIGHT = 0.05;
TILE_GAP = 0.1;
UNIT_HEIGHT = 0.7;

module gem() {
    resize([2*GEM_RADIUS, 2*GEM_RADIUS, GEM_HEIGHT])
        sphere();
}

module tile(colour) {
    difference() {
        union() {
            cube([1, 1, BOARD_THICKNESS], center = true);
            translate([0, 0, BOARD_THICKNESS - tolerance - TILE_HEIGHT / 2])
                cube([1 - TILE_GAP, 1 - TILE_GAP, TILE_HEIGHT], center = true);
        }
        color(colour)
            translate([0, 0, BOARD_THICKNESS + GEM_HEIGHT / 4])
            gem();
    }
}

module board() {
    for (x = [0 : 7]) {
        for (y = [0 : 7]) {
            translate([x - 4, y - 4, 0])
                tile((x+y) % 2 == 0 ? "red" : "blue");
        }
    }
}

module inventory() {
    for (x = [0 : 7]) {
        for (y = [0 : 7]) {
            translate([x - 4, y - 4, 0])
                gem();
        }
    }
}

module zombie() {
    color("red")
        difference() {
            cube([1 - TILE_GAP/2, 1 - TILE_GAP/2, UNIT_HEIGHT], center = true);
            cube([1 - TILE_GAP, 1 - TILE_GAP, TILE_HEIGHT + tolerance], center = true);
        }
}

board();
translate([-10, -10, 0])
    inventory();

zombie();
