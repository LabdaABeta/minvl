#!/usr/bin/env kscript
//INCLUDE minvl.kt

fun main() {
    val state = VL()
    println(r(state))
    println("Valid actions:")

    for (action in VL(state).split(",")) {
        println("Applying $action results in...")
        println(r(VL(state, action)))
    }
}
