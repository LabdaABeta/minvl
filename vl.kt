fun r(s:String):String=s.chunked(16){it.chunked(2){c->"${27.toChar()}[4"+when(c[1]){in "VLHFZA"->0;in "vlhfza"->7;else->3}+"m${27.toChar()}[3"+when(c[1]){in "vV"->1;in "lL"->2;in "hH"->4;in "fFaA"->5;in "zZ"->6;else->0}+"m"+if(c[1]=='.')"×" else if(c[1]in "aA")" ➊➋➌➍➎➏"[c[0]-'0']else " ①②③④⑤⑥⑦⑧⑨⑩"[c[0]-'0']}.joinToString("")}.joinToString("${27.toChar()}[m\n")+"${27.toChar()}[m\n"

fun String.toggleCase() = map {
    when {
        it.isUpperCase() -> it.toLowerCase()
        it.isLowerCase() -> it.toUpperCase()
        else -> it
    }
}.joinToString("")

fun Char.int() = "$this".toInt()

fun VL(s:String?=null,a:String?=null):String=
    if (s == null)
        "2V1Z2V3F3F2V1Z2V2L2L2H2H2H2H2L2L" + "0_".repeat(32) + "2l2l2h2h2h2h2l2l2v1z2v3f3f2v1z2v"
    else if (!"([0-9][_.VLHFZAvlhfza]){64}".toRegex().matches(s))
        ""
    else if (a == null)
        (0..7).flatMap { x -> (0..7).flatMap { y -> ("uUnNrReEdDsSlLwW").flatMap { d -> (0..7).map { l -> "$x$y$d$l" } } } }.filter { VL(s, it) != "" }.distinct().joinToString(",")
    else if (!"[0-7][0-7][uUnNrReEdDsSlLwW][0-7]".toRegex().matches(a))
        ""
    else {
        val x = a[0].int()
        val y = a[1].int()
        val direction = a[2].toUpperCase()
        val split = a[2].isUpperCase()
        val count = a[3].int()

        val dx = when (direction) {
            in "NRE" -> 1
            in "SLW" -> -1
            else -> 0
        }

        val dy = when (direction) {
            in "UNW" -> -1
            in "EDS" -> 1
            else -> 0
        }

        val sourceIdx = 16 * y + 2 * x
        val targetIdx = 16 * (y + dy) + 2 * (x + dx)

        val source = sourceIdx .. sourceIdx + 1
        val target = targetIdx .. targetIdx + 1

        val sourceUnit = s[sourceIdx + 1]
        val sourceSize = s[sourceIdx].int()
        val unit = if (sourceUnit == 'a') 'f' else sourceUnit
        val floor = if (sourceUnit == 'a') '.' else '_'

        if (x !in 0..7 || y !in 0..7 || (x + dx) !in 0..7 || (y + dy) !in 0..7 || sourceUnit !in "vlhfza")
            ""
        else {
            val targetUnit = s[targetIdx + 1]
            val targetSize = s[targetIdx].int()

            if (split) {
                if (sourceSize <= count || count == 0) "" else {
                    val emptied = s.replaceRange(sourceIdx..sourceIdx, "${sourceSize - count}")

                    when (targetUnit) {
                        '_' ->
                            emptied.replaceRange(target, "$count$unit").toggleCase()

                        '.' ->
                            if (sourceUnit !in "fa")
                                ""
                            else
                                emptied.replaceRange(target, "${count}a").toggleCase()

                        else -> ""
                    }
                }
            } else {
                when (count) {
                    0 ->
                        when (sourceUnit) {
                            'l' ->
                                if (targetUnit == '.')
                                    s.replaceRange(target, "0_").toggleCase()
                                else ""

                            'h' ->
                                if (targetUnit == '_')
                                    s.replaceRange(target, "0.").toggleCase()
                                else ""

                            else -> ""
                        }

                    1 -> {
                        val emptied = s.replaceRange(source, "0$floor")

                        when (targetUnit) {
                            '.' ->
                                if (sourceUnit in "fa")
                                    emptied.replaceRange(target, "${sourceSize}a").toggleCase()
                                else
                                    ""

                            '_' ->
                                emptied.replaceRange(target, "$sourceSize$unit").toggleCase()

                            in "vlhfz" ->
                                if (unit == targetUnit)
                                    emptied.replaceRange(target, "${sourceSize + targetSize}$unit").toggleCase()
                                else
                                    ""

                            'a' ->
                                if (unit == 'f')
                                    emptied.replaceRange(target, "${sourceSize + targetSize}a").toggleCase()
                                else
                                    ""

                            in "VLHFZ" -> {
                                val advantage = when (targetUnit) {
                                    'V' -> when (sourceUnit) {
                                        in "hz" -> -1
                                        in "lfa" -> 1
                                        else -> 0
                                    }
                                    'L' -> when (sourceUnit) {
                                        in "vz" -> -1
                                        in "hfa" -> 1
                                        else -> 0
                                    }
                                    'H' -> when (sourceUnit) {
                                        in "lfa" -> -1
                                        in "vz" -> 1
                                        else -> 0
                                    }
                                    'F' -> when (sourceUnit) {
                                        in "vl" -> -1
                                        in "hz" -> 1
                                        else -> 0
                                    }
                                    'Z' -> when (sourceUnit) {
                                        in "hfa" -> -1
                                        in "vl" -> 1
                                        else -> 0
                                    }
                                    else -> 0
                                }

                                val self = sourceSize - when (advantage) {
                                    1 -> targetSize / 2
                                    -1 -> targetSize * 2
                                    else -> targetSize
                                }

                                val enemy = targetSize - when (advantage) {
                                    1 -> sourceSize * 2
                                    -1 -> sourceSize / 2
                                    else -> sourceSize
                                }

                                val root = if (sourceUnit == 'z' && targetUnit == 'H') s else emptied

                                if (self > 0) {
                                    root.replaceRange(target, "$self$unit").toggleCase()
                                } else if (enemy > 0) {
                                    root.replaceRange(target, "$enemy$targetUnit").toggleCase()
                                } else {
                                    root.replaceRange(target, "0_").toggleCase()
                                }
                            }

                            'A' -> {
                                if (sourceUnit in "fa") {
                                    val size = sourceSize - targetSize
                                    if (size < 0)
                                        ""
                                    else if (size == 0)
                                        emptied.replaceRange(target, "0.").toggleCase()
                                    else
                                        emptied.replaceRange(target, "${size}a").toggleCase()
                                } else ""
                            }

                            else -> ""
                        }
                    }

                    else ->
                        if (count > sourceSize + if (sourceUnit == 'v') sourceSize else 0) "" else {
                            val nextAction = "${x+dx}${y+dy}${a[2]}${count - 1}"
                            when (targetUnit) {
                                '.' ->
                                    if (sourceUnit in "fa")
                                        VL(
                                            s.replaceRange(target, "${sourceSize}a").replaceRange(source, "0$floor"),
                                            nextAction
                                        )
                                    else
                                        ""

                                '_' ->
                                    VL(
                                        s.replaceRange(target, "$sourceSize$unit").replaceRange(source, "0$floor"),
                                        nextAction
                                    )

                                else ->
                                    ""
                            }
                        }
                }
            }
        }
    }
