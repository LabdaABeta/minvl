package components

import kotlinx.html.js.*
import kotlinx.html.tabIndex
import org.w3c.dom.events.KeyboardEvent
import org.w3c.dom.events.WheelEvent
import react.*
import react.dom.div
import vl.Action
import vl.Board
import vl.Piece
import vl.Team

interface VLBoardProps : RProps {
    var board : Board
    var team : Team?
    var onAct : (Action) -> Unit
    var canAct : Boolean
    var showAction : Action?
}

data class VLBoardState(
    var selected : Pair<Int, Int>?,
    var split : Int?
) : RState

// TODO: Add "undo/history" menu as a different prop
class VLBoard : RComponent<VLBoardProps, VLBoardState>() {
    init {
        state = VLBoardState(
            selected = null,
            split = null
        )
    }

    private fun clickOn(allActions : List<Action>, coord : Pair<Int, Int>) {
        if (state.selected != null) {
            allActions.find {
                it.source == state.selected && it.finalTarget == coord && it.splitSize == state.split && !it.isSpecial
            }?.let { props.onAct(it) }
            setState { selected = null; split = null }
        } else {
            setState {
                selected = if (isSelectable(allActions, coord)) coord else null
                split = null
            }
        }
    }

    private fun rightClickOn(allActions : List<Action>, coord : Pair<Int, Int>) {
        if (state.selected != null && state.split == null) {
            allActions.find {
                it.source == state.selected && it.finalTarget == coord && it.isSpecial && !it.split
            }?.let { props.onAct(it) } ?: setState { selected = null }
        } else {
            setState {
                selected = if (isSelectable(allActions, coord)) coord else null
                split = props.board[coord]?.size?.minus(1)
            }
        }
    }

    private fun isSelectable(allActions : List<Action>, coord : Pair<Int, Int>) = props.canAct &&
        if (state.selected == null)
            allActions.any { it.source == coord }
        else
            allActions.any { it.source == state.selected && it.finalTarget == coord && it.splitSize == state.split }

    private fun handleScroll(amount : Int) {
        val source = state.selected
        val split = state.split
        if (source != null && split != null) {
            val maxSplit = props.board[source]?.size?.minus(1) ?: 1
            val minSplit = if (props.board[source]?.piece == Piece.HUMAN) 0 else 1

            if (amount < 0 && split < maxSplit) {
                setState { this.split = this.split?.plus(1) }
            } else if (amount > 0 && split > minSplit) {
                setState { this.split = this.split?.minus(1) }
            }
        }
    }

    private fun handleKeypress(key : String) {
        val source = state.selected
        val split = state.split
        if (source != null && split != null) {
            val maxSplit = props.board[source]?.size?.minus(1) ?: 1
            val minSplit = if (props.board[source]?.piece == Piece.HUMAN) 0 else 1

            if (key == "+" && split < maxSplit) {
                setState { this.split = this.split?.plus(1) }
            } else if (key == "-" && split > minSplit) {
                setState { this.split = this.split?.minus(1) }
            }
        }
    }

    override fun RBuilder.render() {
        val board = props.board
        val allActions = board.validActions
        val sourceActions = allActions.filter {
            it.source == state.selected && it.splitSize == state.split
        }

        div("vlboard") {
            attrs.tabIndex = "0"
            attrs.onWheelFunction = { it.preventDefault(); handleScroll(it.asDynamic().deltaY as Int) }
            attrs.onKeyPressFunction = { handleKeypress(it.asDynamic().key as String) }

            val yIndices = if (props.team == Team.WHITE) board.tiles.indices.reversed() else board.tiles.indices
            for (y in yIndices) {
                div("vlrow row$y") {
                    for (x in board.tiles[y].indices) {
                        val coord = x to y
                        val cell = board[coord]
                        val classSet = mutableSetOf(
                            "floor${cell?.floor}",
                            "team${cell?.team}",
                            "size${cell?.size}",
                            "piece${cell?.piece}"
                        )

                        if (isSelectable(allActions, coord)) classSet += "selectable"
                        if (state.selected == coord) classSet += "selected"
                        if (props.showAction?.source == coord) classSet += "lastsource"
                        if (props.showAction?.finalTarget == coord) classSet += "lasttarget"

                        val targetActions = sourceActions.filter { it.finalTarget == coord }

                        val classes = classSet.joinToString(" ")

                        div("vlcell $classes column$x") {
                            attrs {
                                onClickFunction = { clickOn(allActions, coord) }
                                onContextMenuFunction = { it.preventDefault(); rightClickOn(allActions, coord) }
                            }
                            div("vlfloor $classes") { }
                            div("vlteam $classes") { }
                            div("vlsize $classes") { +cell?.size.toString() }
                            div("vlunit $classes") { }
                            for (action in targetActions) {
                                val result = board.apply(action)[coord]
                                val actionClasses = "count${action.count} dir${action.direction} special${action.isSpecial} split${action.split}"
                                val resultClasses = "floor${result?.floor} team${result?.team} size${result?.size} piece${result?.piece}"
                                div("vlaction $actionClasses") { }
                                div("vlresult $resultClasses") {
                                    div("vlfloor $resultClasses") { }
                                    div("vlteam $resultClasses") { }
                                    div("vlsize $resultClasses") { +result?.size.toString() }
                                    div("vlunit $resultClasses") { }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun componentDidUpdate(prevProps: VLBoardProps, prevState: VLBoardState, snapshot: Any) {
        if (prevProps.board != props.board)
            setState { selected = null; split = null }
    }
}

fun RBuilder.vlBoard(handler: RElementBuilder<VLBoardProps>.() -> Unit): ReactElement =
    child<VLBoardProps, VLBoard> { handler() }
