package components

import kotlinx.html.id
import react.*
import react.dom.div

interface VLReferenceProps : RProps {
}

class VLReference : RComponent<VLReferenceProps, RState>() {
    private fun RBuilder.curved_arrow(id : String) {
        div {
            attrs.id = id
            div("arc") {}
            div("head") {}
        }
    }

    override fun RBuilder.render() {
        div {
            attrs.id = "reference"
            // TODO: in the ref node maybe have children for "good vs TYPE" and "bad vs TYPE'

            div { attrs.id = "refvampire"; }
            div { attrs.id = "refleprechaun" }
            div { attrs.id = "refhuman" }
            div { attrs.id = "reffairy" }
            div { attrs.id = "refzombie" }

            curved_arrow("refvampire-beats-human")
            curved_arrow("refvampire-beats-zombie")
            curved_arrow("refleprechaun-beats-vampire")
            curved_arrow("refleprechaun-beats-zombie")
            curved_arrow("refhuman-beats-leprechaun")
            curved_arrow("refhuman-beats-fairy")
            curved_arrow("reffairy-beats-vampire")
            curved_arrow("reffairy-beats-leprechaun")
            curved_arrow("refzombie-beats-human")
            curved_arrow("refzombie-beats-fairy")
        }
    }
}

fun RBuilder.vlReference(handler: RElementBuilder<VLReferenceProps>.() -> Unit): ReactElement =
    child<VLReferenceProps, VLReference> { handler() }
