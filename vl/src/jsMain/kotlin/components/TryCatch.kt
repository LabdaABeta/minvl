package components

import react.*
import react.dom.*

interface TryCatchProps : RProps {
}

class TryCatchState(var message : String? = null) : RState

class TryCatch : RComponent<TryCatchProps, TryCatchState>() {
    override fun TryCatchState.init() {
    }

    override fun RBuilder.render() {
        if (state.message != null) {
            p {
                +(state.message ?: "error")
            }
        } else {
            children()
        }
    }

    override fun componentDidCatch(error: Throwable, info: RErrorInfo) {
        setState {
            message = error.stackTraceToString()
        }
    }
}

fun RBuilder.tryCatch(handler: RElementBuilder<TryCatchProps>.() -> Unit): ReactElement =
    child<TryCatchProps, TryCatch> { handler() }
