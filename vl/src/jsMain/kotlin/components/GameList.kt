package components

import api.GameData
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import react.*
import react.dom.div

interface GameListProps : RProps {
    var games : List<GameData>
}

class GameList : RComponent<GameListProps, RState>() {
    override fun RBuilder.render() {
        for (game in props.games) {
            div { +game.name }
        }
    }
}

fun RBuilder.gameList(handler: RElementBuilder<GameListProps>.() -> Unit): ReactElement =
    child<GameListProps, GameList> { handler() }
