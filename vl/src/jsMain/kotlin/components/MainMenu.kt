package components

import components.widgets.item
import components.widgets.menu
import components.widgets.submenu
import react.*

interface MainMenuProps : RProps {
    var onLogin : () -> Unit
}

interface MainMenuState : RState {
}

class MainMenu : RComponent<MainMenuProps, MainMenuState>() {
    override fun MainMenuState.init() {
    }

    override fun RBuilder.render() {
        menu {
            item {
                attrs.onClick = { props.onLogin() }
                +"Login"
            }
        }
    }
}

fun RBuilder.mainMenu(handler: RElementBuilder<MainMenuProps>.() -> Unit): ReactElement =
    child<MainMenuProps, MainMenu> { handler() }
