package components

import components.widgets.button
import react.*
import react.dom.div
import vl.Action

interface VLHistoryProps : RProps {
    var history : List<Action>
    var current : Int
    var onView : (Int) -> Unit
}

class VLHistory : RComponent<VLHistoryProps, RState>() {
    override fun RBuilder.render() {
        div("history") {
            div("history-items") {
                props.history.forEachIndexed { idx, act ->
                    div("history-item") {
                        if (idx % 2 == 0) {
                            div("history-item-index") { +((idx / 2) + 1).toString() }
                        }
                        div("history-item-item") { +act.toString() }
                    }

                }
                for (action in props.history) {
                    div("history-item") {
                        +action.toString()
                    }
                }
            }

            div("history-buttons") {
                button {
                    attrs.enabled = props.current > props.history.indices.first
                    attrs.onClick = { props.onView(props.history.indices.first) }
                    +"|<"
                }

                button {
                    attrs.enabled = props.current > props.history.indices.first
                    attrs.onClick = { props.onView(props.current - 1) }
                    +"<"
                }

                button {
                    attrs.enabled = props.current < props.history.indices.last
                    attrs.onClick = { props.onView(props.current + 1) }
                    +">"
                }

                button {
                    attrs.enabled = props.current < props.history.indices.last
                    attrs.onClick = { props.onView(props.current + 1) }
                }
            }
        }
    }
}

fun RBuilder.vLHistory(handler: RElementBuilder<VLHistoryProps>.() -> Unit): ReactElement =
    child<VLHistoryProps, VLHistory> { handler() }