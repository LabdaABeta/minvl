package components.widgets

import kotlinx.html.InputType
import kotlinx.html.LABEL
import kotlinx.html.js.onChangeFunction
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.*

interface InputFieldProps : RProps {
    var id : String
    var label : RDOMBuilder<LABEL>.() -> Unit
    var value : String
    var onChange : (String) -> Unit
    var placeholder : String?
}

class InputField : RComponent<InputFieldProps, RState>() {
    override fun RBuilder.render() {
        label {
            attrs.htmlFor = props.id
            props.label.invoke(this)
        }

        input {
            attrs {
                type = InputType.text
                value = props.value
                if (props.placeholder != null) placeholder = props.placeholder!!
                onChangeFunction = { e ->
                    val value = (e.target as? HTMLInputElement)?.value

                    if (value != null)
                        props.onChange(value)
                    else
                        console.log(e)
                }
            }
        }
    }
}

fun RBuilder.inputField(handler: RElementBuilder<InputFieldProps>.() -> Unit): ReactElement =
    child<InputFieldProps, InputField> { handler() }
