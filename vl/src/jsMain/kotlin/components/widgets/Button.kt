package components.widgets

import kotlinx.html.BUTTON
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.RDOMBuilder
import react.dom.button
import react.dom.span

interface ButtonProps : RProps {
    var classes : String?
    var content : (RDOMBuilder<BUTTON>.() -> Unit)?
    var onClick : () -> Unit
    var enabled : Boolean
}

class Button : RComponent<ButtonProps, RState>() {

    override fun RBuilder.render() {
        button(classes = props.classes) {
            attrs.onClickFunction = { _ -> props.onClick() }
            attrs.disabled = !props.enabled
            props.content?.invoke(this)
            props.children()
        }
    }
}

fun RBuilder.button(classes : String? = null, handler: RElementBuilder<ButtonProps>.() -> Unit): ReactElement =
    child<ButtonProps, Button> {
        attrs {
            this.classes = classes
            content = null
            onClick = { }
            enabled = true
        }
        handler()
    }
