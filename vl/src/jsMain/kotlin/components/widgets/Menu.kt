package components.widgets

import kotlinx.browser.document
import kotlinx.html.LI
import kotlinx.html.js.onClickFunction
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.events.Event
import react.*
import react.dom.RDOMBuilder
import react.dom.li
import react.dom.ul

interface MenuProps : RProps {
}

interface MenuState : RState {
    var openIndices : List<Int>
}

class Menu : RComponent<MenuProps, MenuState>() {
    override fun MenuState.init() {
        openIndices = emptyList()
    }

    override fun RBuilder.render() {
        ul("menu") {
            children()
        }
    }

    companion object {
        interface SubmenuProps : RProps {
            var title : RDOMBuilder<LI>.() -> Unit
        }

        interface SubmenuState : RState {
            var open : Boolean
            var ref : Element?
        }

        class Submenu : RComponent<SubmenuProps, SubmenuState>() {
            private val eventCallback : (Event) -> Unit = {
                val isInside = state.ref?.contains(it.target as Node) ?: false
                if (!isInside)
                    setState { open = false }
            }

            override fun SubmenuState.init() {
                open = false
                ref = null
            }

            override fun componentDidMount() {
                document.addEventListener("click", eventCallback)
            }

            override fun componentWillUnmount() {
                document.removeEventListener("click", eventCallback)
            }

            override fun RBuilder.render() {
                li("submenu") {
                    attrs.onClickFunction = { setState { open = !open } }
                    props.title.invoke(this)

                    if (state.open) {
                        ul {
                            children()
                        }
                    }

                    ref { state.ref = it }
                }
            }
        }

        interface MenuItemProps : RProps {
            var onClick : () -> Unit
        }

        class MenuItem : RComponent<MenuItemProps, RState>() {
            override fun RBuilder.render() {
                li {
                    attrs.onClickFunction = { _ -> props.onClick() }
                    children()
                }
            }
        }
    }
}

fun RBuilder.menu(handler: RElementBuilder<MenuProps>.() -> Unit): ReactElement =
    child<MenuProps, Menu> { handler() }

fun RElementBuilder<MenuProps>.submenu(handler : RElementBuilder<Menu.Companion.SubmenuProps>.() -> Unit): ReactElement =
    child<Menu.Companion.SubmenuProps, Menu.Companion.Submenu> { handler() }

fun RElementBuilder<MenuProps>.item(handler : RElementBuilder<Menu.Companion.MenuItemProps>.() -> Unit): ReactElement =
    child<Menu.Companion.MenuItemProps, Menu.Companion.MenuItem> { handler() }

fun RElementBuilder<Menu.Companion.SubmenuProps>.item(handler : RElementBuilder<Menu.Companion.MenuItemProps>.() -> Unit): ReactElement =
    child<Menu.Companion.MenuItemProps, Menu.Companion.MenuItem> { handler() }

/* Sample usage:

menu {
    submenu {
        +"A"

        item {
            +"A1"
            action {
                to the thing
            }
        }
    }
}

output

ul {
    ul {
        onclick = {open it up}


 */