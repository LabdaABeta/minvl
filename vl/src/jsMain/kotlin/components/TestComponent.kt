package components

import components.widgets.Button
import components.widgets.button
import react.*
import react.dom.p
import vl.Action
import vl.Board
import vl.Team

interface TestComponentProps : RProps {
    var ai : (Board) -> Action
}

interface TestComponentState : RState {
    var board : Board
    var last : Action?
}

class TestComponent : RComponent<TestComponentProps, TestComponentState>() {
    override fun TestComponentState.init() {
        board = Board.INITIAL_BOARD
        last = null
    }

    override fun RBuilder.render() {
        vlBoard {
            attrs {
                board = state.board
                team = Team.BLACK
                onAct = {
                    console.log("Applying $it to ${state.board}")
                    setState {
                        this.board = this.board.apply(it)
                        last = it
                    }
                }
                canAct = state.board.current == Team.BLACK
                showAction = state.last
            }
        }
        if (state.board.validActions.isNotEmpty()) {
            button {
                attrs.onClick = {
                    setState {
                        val act = props.ai(board)
                        board = board.apply(act)
                        last = act
                    }
                }
                attrs.enabled = state.board.current == Team.WHITE
                +"Move"
            }
        }
    }
}

fun RBuilder.testComponent(handler: RElementBuilder<TestComponentProps>.() -> Unit): ReactElement =
    child<TestComponentProps, TestComponent> { handler() }
