import components.*
import components.widgets.*
import react.dom.render
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.id
import react.RProps
import react.dom.div
import react.dom.li
import react.dom.p
import vl.Board
import vl.Team

fun main() {
    window.onload = {
        render(document.getElementById("root")) {
            // child(Welcome::class) { attrs { name = "Kotlin/JS" } }
            tryCatch {
                mainMenu {
                    attrs.onLogin = { console.log("Logged in") }
                }

                testComponent {
                    attrs.ai = {
                        it.validActions.maxesBy { act ->
                            it.apply(act).tiles.flatten().sumBy { tile ->
                                if (tile.team == Team.WHITE)
                                    tile.size
                                else
                                    -tile.size
                            }
                        }.random()
                    }
                }

                vlReference {  }

                menu {
                    submenu {
                        attrs.title = { +"A" }
                        item {
                            attrs.onClick = { console.log("Aa") }
                            +"a"
                        }
                        item {
                            attrs.onClick = { console.log("Ab") }
                            +"b"
                        }
                    }
                    submenu {
                        attrs.title = { +"B" }
                        item {
                            attrs.onClick = { console.log("Ba") }
                            +"a"
                        }
                        item {
                            attrs.onClick = { console.log("Bb") }
                            +"b"
                        }
                    }
                    item {
                        attrs.onClick = { console.log("a") }
                        +"a"
                    }
                }
            }
        }
    }
}
