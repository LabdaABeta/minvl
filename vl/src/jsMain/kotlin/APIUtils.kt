import kotlinx.browser.window
import kotlinx.coroutines.await
import org.w3c.dom.url.URL
import org.w3c.fetch.Headers
import org.w3c.fetch.Request

object APIUtils {
    const val ENDPOINT = "http://localhost:8080"

    var token : String? = null

    suspend inline fun<T> get(path : String, queryParams : Map<String, String> = emptyMap(), fill : Request.() -> Unit = { }) : T? {
        val url = URL("$ENDPOINT$path")

        for ((k, v) in queryParams) {
            url.searchParams.set(k, v)
        }
        val request = Request(url.toString())
        request.fill()

        val response = window.fetch(request)
        return response.then {
            if (!it.ok) null else {
                JSON.parse<T>(it.body as String)
            }
        }.await()
    }
}