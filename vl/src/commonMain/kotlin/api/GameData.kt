package api

import vl.Action
import vl.Board

data class GameData(
    val id : Int,
    val white : Int?,
    val black : Int?,
    val name : String,
    val board : String,
    val history : String
)