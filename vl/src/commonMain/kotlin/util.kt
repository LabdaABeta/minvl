fun String.toggleCase() = map {
    when {
        it == it.toUpperCase() -> it.toLowerCase()
        it == it.toLowerCase() -> it.toUpperCase()
        else -> it
    }
}.joinToString("")

fun Char.int() = "$this".toInt()

fun<T, R : Comparable<R>> List<T>.maxesBy(mapper : (T) -> R) : List<T> =
    fold(Pair<R?,List<T>>(null, emptyList())) { acc : Pair<R?, List<T>>, item : T ->
        val score = mapper(item)
        val best = acc.first

        when {
            best == null || best < score -> Pair(score, listOf(item))
            best == score -> Pair(score, acc.second + item)
            else -> acc
        }
    }.second

fun<T, R : Comparable<R>> List<T>.minsBy(mapper : (T) -> R) : List<T> =
    fold(Pair<R?,List<T>>(null, emptyList())) { acc : Pair<R?, List<T>>, item : T ->
        val score = mapper(item)
        val best = acc.first

        when {
            best == null || best > score -> Pair(score, listOf(item))
            best == score -> Pair(score, acc.second + item)
            else -> acc
        }
    }.second
