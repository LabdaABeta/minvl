package vl

data class Board(
    val current : Team?,
    val tiles : List<List<Tile>>
) {
    val winner : Team? get() {
        if (tiles.isEmpty()) return current
        val living = tiles.flatten().filter { it.piece == Piece.KING }.map { it.team }.toSet()
        return if (Team.BLACK in living) {
            if (Team.WHITE in living)
                null
            else
                Team.BLACK
        } else {
            if (Team.WHITE in living)
                Team.WHITE
            else
                null
        }
    }

    val gameOver get() = winner != null || tiles.isEmpty() || validActions.isEmpty()

    constructor(contents : String) : this(
        current = Team.values().find { it.shortName == contents.elementAtOrNull(0) },
        tiles = contents.drop(1).chunked(2).chunked(8).map { row -> row.map { Tile(it) } }
    )

    operator fun get(coord : Pair<Int, Int>) : Tile? =
        tiles.elementAtOrNull(coord.second)?.elementAtOrNull(coord.first)

    override fun toString(): String =
        "${current?.shortName ?: ""}" + tiles.joinToString("") { row ->
            row.joinToString("") {
                it.toString()
            }
        }

    operator fun set(coord : Pair<Int, Int>, tile : Tile) : Board =
        copy(
            tiles = tiles.mapIndexed { y, row ->
                if (y == coord.second) {
                    row.mapIndexed { x, cell ->
                        if (x == coord.first) tile else cell
                    }
                } else row
            }
        )

    // Yes, a board can equal a string "" == GAME_OVER
    override fun equals(other: Any?): Boolean = toString() == other.toString()

    override fun hashCode(): Int = toString().hashCode()

    private fun endTurn() : Board = copy(current = if (current == Team.BLACK) Team.WHITE else Team.BLACK)

    private val allActions : List<Action> get() =
        if (winner == null) {
            (0..7).flatMap { x ->
                (0..7).flatMap { y ->
                    // This is an optimization to cut down on the number of possible actions to filter
                    if (tiles[y][x].team == current) {
                        ("uUnNrReEdDsSlLwW").flatMap { d ->
                            (0..7).map { l ->
                                Action("$x$y$d$l")
                            }
                        }
                    } else {
                        emptyList()
                    }
                }
            }
        } else emptyList()

    val validActions : List<Action> get() =
        allActions.filter { apply(it).tiles.isNotEmpty() }.distinct()

    private fun applySplit(
        source : Tile,
        target : Tile,
        action : Action,
    ) : Board =
        if (source.size <= action.count || action.count < 0 || target.size != 0 || (action.count == 0 && source.piece != Piece.HUMAN))
            WON_BOARD[source.team?.enemy] ?: GAME_OVER
        else {
            val emptied = set(action.source, source.copy(size = source.size - action.count))

            if (target.floor) {
                if (action.count > 0) {
                    emptied.set(action.target, Tile(true, action.count, source.team, source.piece)).endTurn()
                } else {
                    if (source.piece == Piece.HUMAN)
                        emptied.set(action.target, Tile(true, 1, source.team, Piece.ZOMBIE)).endTurn()
                    else
                        WON_BOARD[source.team?.enemy] ?: GAME_OVER
                }
            } else {
                if (source.piece == Piece.FAIRY && action.count > 0)
                    emptied.set(action.target, Tile(false, action.count, source.team, source.piece)).endTurn()
                else
                    WON_BOARD[source.team?.enemy] ?: GAME_OVER
            }
        }

    private fun applySpecial(
        source : Tile,
        target : Tile,
        action : Action
    ) : Board =
        if (target.piece != null)
            WON_BOARD[source.team?.enemy] ?: GAME_OVER
        else {
            when (source.piece) {
                Piece.LEPRECHAUN ->
                    if (!target.floor)
                        set(action.target, Tile(true, 0, null, null)).endTurn()
                    else
                        WON_BOARD[source.team?.enemy] ?: GAME_OVER

                Piece.HUMAN ->
                    if (target.floor)
                        set(action.target, Tile(false, 0, null, null)).endTurn()
                    else
                        WON_BOARD[source.team?.enemy] ?: GAME_OVER

                Piece.KING ->
                    if (target.floor)
                        set(action.target, Tile(false, 0, null, null)).endTurn()
                    else
                        set(action.target, Tile(true, 0, null, null)).endTurn()

                else ->
                    WON_BOARD[source.team?.enemy] ?: GAME_OVER
            }
        }

    private fun applyEmptyMove(
        source : Tile,
        target : Tile,
        action : Action,
        emptied : Board
    ) : Board =
        when {
            target.floor -> emptied.set(action.target, source.copy(floor = true)).endTurn()
            source.piece == Piece.FAIRY -> emptied.set(action.target, source.copy(floor = false)).endTurn()
            else -> WON_BOARD[source.team?.enemy] ?: GAME_OVER
        }

    private fun applyMerge(
        source : Tile,
        target : Tile,
        action : Action,
        emptied : Board
    ) : Board =
        if (source.piece == target.piece && (target.floor || source.piece == Piece.FAIRY) && source.piece != Piece.ZOMBIE)
            emptied.set(action.target, target.copy(size = source.size + target.size)).endTurn()
        else
            WON_BOARD[source.team?.enemy] ?: GAME_OVER

    private fun applyNormalCombat(
        source : Tile,
        target : Tile,
        action : Action,
        emptied : Board
    ) : Board = run {
        val advantage = source.piece!!.advantage(target.piece!!)
        val self = source.size - when (advantage) {
            1 -> target.size / 2
            -1 -> target.size * 2
            else -> target.size
        }
        val enemy = target.size - when (advantage) {
            1 -> source.size * 2
            -1 -> source.size / 2
            else -> source.size
        }

        emptied.set(
            action.target,
            when {
                source.piece == Piece.ZOMBIE && target.piece == Piece.HUMAN ->
                    target.copy(size = 1, team = current, piece = Piece.ZOMBIE)
                self > 0 ->
                    target.copy(size = self, team = current, piece = source.piece)
                enemy > 0 ->
                    target.copy(size = enemy)
                else ->
                    Tile(true, 0, null, null)
            }
        ).endTurn()
    }

    private fun applyAerialCombat(
        source : Tile,
        target : Tile,
        action : Action,
        emptied : Board
    ) : Board =
        if (source.piece == Piece.FAIRY) {
            val size = source.size - target.size

            emptied.set(action.target,
                when {
                    size < 0 -> target.copy(size = -size)
                    size == 0 -> target.emptied()
                    size > 0 -> source.copy(floor = target.floor, size = size)
                    else -> error("Unreachable")
                }
            ).endTurn()
        } else WON_BOARD[source.team?.enemy] ?: GAME_OVER

    private fun applyCombat(
        source : Tile,
        target : Tile,
        action : Action,
        emptied : Board
    ) : Board =
        when {
            target.piece == null -> applyEmptyMove(source, target, action, emptied)
            target.team == current -> applyMerge(source, target, action, emptied)
            target.floor -> applyNormalCombat(source, target, action, emptied)
            else -> applyAerialCombat(source, target, action, emptied)
        }

    private fun applyMove(
        source : Tile,
        target : Tile,
        action : Action,
    ) : Board =
        when {
            action.count > source.size + if (source.piece == Piece.VAMPIRE) source.size else 0 -> WON_BOARD[source.team?.enemy] ?: GAME_OVER
            target.piece != null -> WON_BOARD[source.team?.enemy] ?: GAME_OVER
            !target.floor && source.piece != Piece.FAIRY -> WON_BOARD[source.team?.enemy] ?: GAME_OVER
            else -> set(action.target, source.copy(floor = target.floor)).set(action.source, source.emptied()).apply(action.copy(source = action.target, count = action.count - 1))
        }

    fun apply(action : Action) : Board {
        val source = get(action.source)!! // <-- Always safe if action is valid (0-7 enforced for x and y)
        val target = get(action.target)

        // Only the current team can act, and only within the grid
        if (source.team != current || source.piece == null || target == null)
            return WON_BOARD[source.team?.enemy] ?: GAME_OVER

        return if (action.split) {
            applySplit(source, target, action)
        } else {
            when (action.count) {
                0 -> applySpecial(source, target, action)
                1 -> applyCombat(source, target, action, set(action.source, source.emptied()))
                else -> applyMove(source, target, action)
            }
        }
    }

    companion object {
        fun isValidBoard(representation : String) : Boolean =
            "[wb]([0-9][_.VLHFZAvlhfza]){64}".toRegex().matches(representation)

        val INITIAL_BOARD : Board = Board("""
            b
            3A2K2V2V2V2V0.3A
            0.0.4L4L4H4H0.0.
            0_0_0_0_0_0_0_0_
            0_0_0_0_0_0_0_0_
            0_0_0_0_0_0_0_0_
            0_0_0_0_0_0_0_0_
            0.0.4h4h4l4l0.0.
            3a0.2v2v2v2v2k3a
        """.trimIndent().filter { !it.isWhitespace() })
        val GAME_OVER : Board = Board("")
        val WON_BOARD : Map<Team, Board> = mapOf(Team.WHITE to Board("w"), Team.BLACK to Board("b"))
    }
}

// b2V1Z2V3F3F2V1Z2V2L2L2H2H2H2H2L2L0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2l2l2h2h2h2h2l2l2v1z2v3f3f2v1z2v
