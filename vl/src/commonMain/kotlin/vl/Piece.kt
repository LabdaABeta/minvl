package vl

enum class Piece(val shortName : Char) {
    VAMPIRE('V'),
    LEPRECHAUN('L'),
    HUMAN('H'),
    FAIRY('F'),
    ZOMBIE('Z'),
    KING('K');

    fun advantage(against : Piece) : Int =
        when (this) {
            KING -> ADVANTAGE
            against -> NEUTRAL
            VAMPIRE -> if (against in setOf(KING, HUMAN, ZOMBIE)) ADVANTAGE else DISADVANTAGE
            LEPRECHAUN -> if (against in setOf(KING, VAMPIRE, ZOMBIE)) ADVANTAGE else DISADVANTAGE
            HUMAN -> if (against in setOf(KING, LEPRECHAUN, FAIRY)) ADVANTAGE else DISADVANTAGE
            FAIRY -> if (against in setOf(KING, VAMPIRE, LEPRECHAUN)) ADVANTAGE else DISADVANTAGE
            ZOMBIE -> if (against in setOf(KING, HUMAN, FAIRY)) ADVANTAGE else DISADVANTAGE
        }

    companion object {
        const val ADVANTAGE = 1
        const val NEUTRAL = 0
        const val DISADVANTAGE = -1
    }
}