package vl

enum class Team(val shortName : Char) {
    WHITE('w'), BLACK('b');

    val enemy get() = if (this == WHITE) BLACK else WHITE
}