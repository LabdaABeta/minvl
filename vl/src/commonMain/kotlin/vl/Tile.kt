package vl

data class Tile(
    val floor : Boolean,
    val size : Int, // Limited to [0,8]
    val team : Team?,
    val piece : Piece?
) {
    constructor(contents: String) : this(
        floor = contents[1] in "VLHFZKvlhfzk_",
        size = "${contents[0]}".toInt(),
        team = when (contents[1]) {
            in "VLHFZAK" -> Team.WHITE
            in "vlhfzak" -> Team.BLACK
            else -> null
        },
        piece = when (contents[1]) {
            in "vV" -> Piece.VAMPIRE
            in "lL" -> Piece.LEPRECHAUN
            in "hH" -> Piece.HUMAN
            in "fFaA" -> Piece.FAIRY
            in "zZ" -> Piece.ZOMBIE
            in "kK" -> Piece.KING
            else -> null
        }
    )

    private val floorSymbol: Char =
        if (floor) {
            when (piece) {
                null -> '_'
                Piece.VAMPIRE -> if (team == Team.WHITE) 'V' else 'v'
                Piece.LEPRECHAUN -> if (team == Team.WHITE) 'L' else 'l'
                Piece.HUMAN -> if (team == Team.WHITE) 'H' else 'h'
                Piece.FAIRY -> if (team == Team.WHITE) 'F' else 'f'
                Piece.ZOMBIE -> if (team == Team.WHITE) 'Z' else 'z'
                Piece.KING -> if (team == Team.WHITE) 'K' else 'k'
            }
        } else {
            when (piece) {
                null -> '.'
                Piece.FAIRY -> if (team == Team.WHITE) 'A' else 'a'
                else -> '?'
            }
        }

    fun emptied() : Tile = copy(size = 0, team = null, piece = null)

    override fun toString(): String = "$size$floorSymbol"

    companion object {
        fun isValidTile(representation : String) : Boolean =
            "[0-9][_.VLHFZAKvlhfzak]".toRegex().matches(representation)
    }
}
