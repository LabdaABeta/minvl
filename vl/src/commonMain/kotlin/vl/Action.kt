package vl

data class Action(
    val source : Pair<Int, Int>,
    val direction : Direction,
    val split : Boolean,
    val count : Int, // limited to [0,8]
    private val dirchar : Char
) {
    constructor(contents : String) : this(
        source = Pair("${contents[0]}".toInt(), "${contents[1]}".toInt()),
        direction = Direction.values().find { it.shortName == contents[2].toUpperCase() }!!,
        split = contents[2] in "UNREDSLW",
        count = "${contents[3]}".toInt(),
        dirchar = contents[2]
    )

    val isSpecial : Boolean = (count == 0 && !split)
    val target : Pair<Int, Int> =
        Pair(source.first + direction.dx, source.second + direction.dy)

    val distance : Int = if (split || isSpecial) 1 else count

    val finalTarget : Pair<Int, Int> =
        Pair(source.first + direction.dx * distance, source.second + direction.dy * distance)

    val splitSize : Int? = if (split) count else null

    override fun toString(): String =
        "${source.first}${source.second}${dirchar}${count}"

    companion object {
        fun isValidAction(representation : String) : Boolean =
            "[0-7][0-7][uUnNrReEdDsSlLwW][0-7]".toRegex().matches(representation)
    }
}

