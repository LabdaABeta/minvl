package vl

// Directions are given names that are distinguisable at the 1 character level as:
// W U N
// L   R
// S D E
enum class Direction(val shortName : Char, val dx : Int, val dy : Int) {
    UP('U', 0, -1),
    NORTH('N', 1, -1),
    RIGHT('R', 1, 0),
    EAST('E', 1, 1),
    DOWN('D', 0, 1),
    SOUTH('S', -1, 1),
    LEFT('L', -1, 0),
    WEST('W', -1, -1)
}
