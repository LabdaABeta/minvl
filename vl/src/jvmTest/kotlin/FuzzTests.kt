import vl.Action
import vl.Board
import vl.minvl.VL
import kotlin.test.*

class FuzzTests {
    private fun canonicalize(boardString : String) : String =
        when {
            boardString == "" -> boardString
            boardString.length == 1 -> ""
            boardString[0] == 'w' -> boardString.drop(1).toggleCase()
            else -> boardString.drop(1)
        }

    @Test
    fun `Fuzz test by applying random moves until game over`() {
        var state = Board.INITIAL_BOARD

        while (!state.gameOver) {
            val actions = state.validActions

            val actionList = actions.sortedBy { it.toString() }
            val index = actions.indices.random()
            val action = actionList[index]
            state = state.apply(actions[index])
        }

        assertEquals(state.validActions, emptyList())
    }
}