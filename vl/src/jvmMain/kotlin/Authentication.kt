import data.objects.Player
import data.tables.Players
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import kotlin.concurrent.fixedRateTimer

// TODO: XXX: Use *real* authentication systems!
// TODO: Eventually add oauth2 support
// TODO: Maintain "insecure" custom-built as an option (like CWS)
object Authentication {
    // Tokens good for 6 hours
    private const val TOKEN_DURATION_MILLISECONDS : Long = 1000 * 60 * 60 * 6

    // TODO: XXX: USE A REAL SECURE HASH! THIS IS WOEFULLY INADEQUATE!
    // SUGGEST USING SCRYPT OR PERHAPS PBKDF2 OR BCRYPT
    @WarnMe("THIS IS NOT SECURE!!!")
    fun hash(content: String): String = "${content.hashCode()}"

    // TODO: XXX: USE A REAL SECURE HASH! THIS IS WOEFULLY INADEQUATE!
    @WarnMe("THIS IS NOT SECURE!!!")
    fun secureRandomString() : String =
        (0..100).map { (('A'..'Z') + ('a'..'z')+('0'..'9')).random() }.joinToString("")

    // Returns the player id or null if not valid
    fun validateUsernameAndPassword(user: String, pass: String): Int? =
        transaction {
            Player.find { Players.name eq user }.map {
                Pair(it.id.value, Pair(it.hashedAndSaltedPassword, it.salt))
            }
        }.run {
            firstOrNull {
                it.second.first == hash(pass + it.second.second)
            }?.first
        }

    // TODO: XXX: This somewhat defeats the purpose of tokens
    // Eventually move this to encrypted and/or signed stateless tokens
    private val tokens : ConcurrentMap<String, Pair<Int, Long>> = ConcurrentHashMap()

    suspend fun createToken(id : Int) : String {
        var result = secureRandomString()

        while (result in tokens)
            result = secureRandomString()

        tokens[result] = Pair(id, System.currentTimeMillis() + TOKEN_DURATION_MILLISECONDS)
        return result
    }

    fun validateToken(token : String) : Int? {
        val result = tokens[token] ?: return null

        if (result.second < System.currentTimeMillis()) {
            tokens.remove(token)
            return null
        }

        return result.first
    }

    init {
        fixedRateTimer(name = "Token Culler", period = TOKEN_DURATION_MILLISECONDS) {
            val now = System.currentTimeMillis()
            tokens.filterValues { it.second < now }.forEach { tokens.remove(it.key) }
        }
    }
}