package data.objects

import CRUDResponse
import api.GameData
import asCRUDResponse
import data.tables.Games
import io.ktor.application.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.transactions.transaction
import pages.*
import routeCreate
import routeRead
import routeUpdate
import vl.Action
import vl.Board
import vl.Team
import kotlin.reflect.jvm.reflect

class Game(id : EntityID<Int>) : Entity<Int>(id) {
    var white by Player optionalReferencedOn Games.white
    var black by Player optionalReferencedOn Games.black
    var name by Games.name
    var board by Games.board
    var history by Games.history

    val raw get() = GameData(id.value, white?.id?.value, black?.id?.value, name, board, history)

    companion object : EntityClass<Int, Game>(Games) {
        fun getAllGames(start : Long = 0, count : Int = 25) =
            transaction {
                all()
                    .limit(count, start)
                    .map { Pair(it.id.value, it.name) }
            }.asCRUDResponse()

        private fun gameById(id : Int) =
            transaction {
                Game.find { Games.id eq id }.map { it.raw }
            }.run {
                asCRUDResponse(if (isEmpty()) HttpStatusCode.NotFound else HttpStatusCode.OK)
            }

        private fun newGame(token : String, name : String = "game", asWhite : Boolean = false) : CRUDResponse {
            val playerId = Authentication.validateToken(token) ?: return unauthorizedHtmlResponse(token)

            return transaction {
                val player = Player.findById(playerId)

                if (player == null)
                    CRUDResponse.HtmlResponse(HttpStatusCode.NotFound) { playerNotFound(playerId) }
                else
                    Game.new {
                        if (asWhite) white = player else black = player
                        this.name = name
                        board = Board.INITIAL_BOARD.toString()
                        history = ""
                    }.asCRUDResponse()
            }
        }

        fun joinGame(token : String, game : Int) : CRUDResponse {
            val playerId = Authentication.validateToken(token) ?: return unauthorizedHtmlResponse(token)

            return transaction {
                val item = Game.findById(game)
                val player = Player.findById(playerId)

                if (item == null)
                    CRUDResponse.HtmlResponse(HttpStatusCode.NotFound) { gameNotFound(game) }
                else if (player == null)
                    CRUDResponse.HtmlResponse(HttpStatusCode.Unauthorized) { playerNotFound(playerId) }
                else {
                    when {
                        item.black == null -> {
                            item.black = player
                            player.name.asCRUDResponse()
                        }
                        item.white == null -> {
                            item.white = player
                            player.name.asCRUDResponse()
                        }
                        else -> {
                            CRUDResponse.HtmlResponse(HttpStatusCode.Forbidden) { gameFull(game) }
                        }
                    }
                }
            }
        }

        private fun myGames(token : String) : CRUDResponse {
            val playerId = Authentication.validateToken(token) ?: return unauthorizedHtmlResponse(token)

            return transaction {
                Game.find { (Games.white eq playerId) or (Games.black eq playerId) }.map { it.raw }
            }.asCRUDResponse()
        }

        private fun makeMove(token : String, game : Int, move : String) : CRUDResponse {
            val playerId = Authentication.validateToken(token) ?: return unauthorizedHtmlResponse(token)

            if (!Action.isValidAction(move))
                return CRUDResponse.HtmlResponse(HttpStatusCode.BadRequest) { badVLAction(move) }

            val action = Action(move)

            return transaction {
                val item = Game.findById(game)
                val player = Player.findById(playerId)

                if (item == null)
                    CRUDResponse.HtmlResponse(HttpStatusCode.NotFound) { gameNotFound(game) }
                else if (player == null)
                    CRUDResponse.HtmlResponse(HttpStatusCode.Unauthorized) { playerNotFound(playerId) }
                else if (item.black == null || item.white == null)
                    CRUDResponse.HtmlResponse(HttpStatusCode.Conflict) { gameNotStarted(game) }
                else {
                    val board = Board(item.board)

                    val currentPlayer = when (board.current) {
                        Team.BLACK -> item.black
                        Team.WHITE -> item.white
                        else -> null
                    }

                    when {
                        currentPlayer == null -> CRUDResponse.HtmlResponse(HttpStatusCode.Conflict) { gameIsOver(game) }
                        currentPlayer != player -> CRUDResponse.HtmlResponse(HttpStatusCode.Forbidden) { notYourTurn(game) }
                        else -> {
                            val newBoard = board.apply(action)
                            item.board = newBoard.toString()
                            newBoard.asCRUDResponse()
                        }
                    }
                }
            }
        }

        fun route(routing: Routing) {
            with (routing) {
                routeRead("/games", ::getAllGames)
                routeRead("/game/{id}", ::gameById)
                routeRead("/game/mine", ::myGames)
                routeCreate("/game/new", ::newGame)
                routeUpdate("/game/join", ::joinGame)
                routeUpdate("/game/move", ::makeMove)
            }
        }
    }
}