package data.objects

import CRUDResponse
import asCRUDResponse
import data.tables.Players
import io.ktor.application.*
import io.ktor.routing.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.html.*
import io.ktor.http.*
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.transactions.transaction
import pages.duplicateName
import pages.missingParam
import pages.unauthorized
import pages.unauthorizedHtmlResponse
import routeCreate
import Authentication
import routeRead

// TODO: Add ability to generate random users with null passwords? "guest" accounts
class Player(id : EntityID<Int>) : Entity<Int>(id) {
    var hashedAndSaltedPassword by Players.hashedAndSaltedPassword
    var salt by Players.salt
    var name by Players.name
    var email by Players.email


    companion object : EntityClass<Int, Player>(Players) {
        val unisexNamesFromWikipedia = setOf(
            "Addison", "Aidan", "Aiden", "Ainslie", "Alex", "Alexis", "Ali", "Allyn", "Angel", "Arden", "Asher",
            "Ashton", "Ashley", "Aston", "Aubrey", "Audie", "Avery", "Bailey", "Beverly", "Billie", "Blair", "Blake",
            "Bobby", "Braidy", "Brett", "Brooklyn", "Brooke", "Cameron", "Carey", "Cary", "Carsen", "Carson", "Casey",
            "Kasey", "Charlie", "Charley", "Chevy", "Chris", "Cody", "Corrie", "Corry", "Cory", "Courtney", "Cree",
            "Kree", "Dakota", "Dallas", "Danny", "Danni", "Darby", "Darcy", "Darian", "Delaney", "Dell", "Devin",
            "Devon", "Dorian", "Drew", "Dylan", "Elliot", "Elis", "Ellis", "Emerson", "Emery", "Erin", "Evan", "Finley",
            "Finn", "Frankie", "Freddie", "Flynn", "Gail", "Gale", "Gayle", "Gerrie", "Gwyn", "Gwynn", "Gwynne",
            "Hadley", "Halsey", "Harley", "Haiden", "Hayden", "Hailey", "Haley", "Halley", "Hayley", "Hilary",
            "Hillary", "Hollis", "Hudson", "Jacy", "Jaime", "Jamie", "Jayme", "Jean", "Jeramie", "Jeramy", "Jeri",
            "Jerry", "Jeryl", "Jeryn", "Jesse", "Jo", "Jocelyn", "Joey", "Jordan", "Jude", "Justice", "Kai", "Kye",
            "Kary", "Kay", "Keegan", "Kelly", "Kendall", "Kerry", "Kim", "Kirby", "Kit", "Kyrie", "Lane", "Laurel",
            "Laurence", "Laurie", "Lea", "Lee", "Leighton", "Lennox", "Leslie", "Lesley", "Lin", "Lyn", "Lynn",
            "Lindsay", "Lindsey", "Lindy", "Logan", "Loren", "Lucky", "Mackenzie", "Madison", "Maddox", "Madox",
            "Marian", "Marion", "Marlee", "Marley", "Marlo", "Marlowe", "Mason", "Meade", "Mel", "Meredith", "Merle",
            "Micah", "Milo", "Montana", "Morgan", "Murphy", "Nash", "Nevada", "Nicky", "Nikki", "Odell", "Ora", "Paige",
            "Peyton", "Palmer", "Parker", "Paris", "Parris", "Pat", "Paxton", "Payton", "Peyton", "Quinn", "Randi",
            "Randy", "Reagan", "Regan", "Rennie", "Renée", "Reed", "Reese", "Ricky", "Ricki", "Riley", "Ridley",
            "Ripley", "Robin", "Robyn", "Rory", "Rowan", "Royce", "Rudy", "Russi", "Ryan", "Rylan", "Rynn", "Sam",
            "Sandy", "Sasha", "Sawyer", "Schuyler", "Skyler", "Skylar", "Scout", "Selby", "Shane", "Shannon", "Shawn",
            "Shay", "Shelby", "Shelley", "Sheridan", "Shirley", "Sidney", "Sydney", "Skeeter", "Spencer", "Storm",
            "Stormy", "Tanner", "Taran", "Tatum", "Taylor", "Tegan", "Temple", "Terry", "Tobey", "Toby", "Tommie",
            "Toni", "Tony", "Torrance", "Tori", "Tory", "Torrey", "Torry", "Tracy", "Tristan", "Tyler", "Tyran",
            "Valentine", "Vivian", "Wallis", "Waverly", "Willie", "Winnie", "Wyatt", "Zane"
        )

        suspend fun login(user : String, pass : String) : CRUDResponse {
            val userId = Authentication.validateUsernameAndPassword(user, pass) ?: return unauthorizedHtmlResponse(user)

            return Authentication.createToken(userId).asCRUDResponse()
        }

        suspend fun guest() : CRUDResponse {
            val user = unisexNamesFromWikipedia.random() + "#" + (0 .. 4).map { (0 .. 9).random() }.joinToString("")

            val idx = transaction {
                if (Player.find { Players.name eq user }.empty()) {
                    Player.new {
                        name = user
                        salt = "guest"
                        hashedAndSaltedPassword = ""
                    }.id.value
                } else {
                    Player.find { Players.name eq user }.first().id.value
                }
            }

            return Authentication.createToken(idx).asCRUDResponse()
        }

        fun newUser(username : String, pass : String, email : String? = null) : CRUDResponse {
            val user = username
            return transaction {
                if (Player.find { Players.name eq user }.empty()) {
                    val sel = Authentication.secureRandomString()
                    Player.new {
                        name = user
                        salt = sel
                        hashedAndSaltedPassword = Authentication.hash(pass + sel)
                        this.email = email
                    }.id.value.asCRUDResponse()
                } else {
                    CRUDResponse.HtmlResponse(HttpStatusCode.Conflict) { duplicateName(user) }
                }
            }
        }

        fun checkName(name : String = "") : CRUDResponse =
            transaction {
                Player.find { Players.name eq name }.empty()
            }.asCRUDResponse()

        // TODO: Forgot password (reset to random and send in an email)

        fun route(routing: Routing) {
            with (routing) {
                routeRead("/checkName", ::checkName)
                routeCreate("/login", ::login)
                routeCreate("/createUser", ::newUser)
                routeCreate("/guest", ::guest)
            }
        }
    }
}