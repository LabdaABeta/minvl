package data.tables

import org.jetbrains.exposed.dao.id.IntIdTable

object Players : IntIdTable() {
    val hashedAndSaltedPassword = text("key")
    val salt = text("salt")
    val name = text("name")
    val email = text("email").nullable()
}