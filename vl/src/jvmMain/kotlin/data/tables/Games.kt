package data.tables

import org.jetbrains.exposed.dao.id.IntIdTable

object Games : IntIdTable() {
    val white = reference("white", Players).nullable()
    val black = reference("black", Players).nullable()
    val name = text("name")
    val board = text("board")
    val history = text("history")
}