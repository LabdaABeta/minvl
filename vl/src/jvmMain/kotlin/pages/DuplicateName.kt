package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.duplicateName(name : String) {
    head {
        title("Name $name already taken")
    }
    body {
        +"409"
    }
}