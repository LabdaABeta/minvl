package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.gameNotFound(id : Int) {
    head {
        title("Game with id $id not found")
    }
    body {
        +"404"
    }
}
