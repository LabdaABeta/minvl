package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.gameFull(id : Int) {
    head {
        title("Game with id $id is full")
    }
    body {
        +"404"
    }
}
