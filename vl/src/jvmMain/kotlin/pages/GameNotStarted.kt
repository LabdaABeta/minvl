package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.gameNotStarted(id : Int) {
    head {
        title("Game with id $id hasn't started")
    }
    body {
        +"404"
    }
}
