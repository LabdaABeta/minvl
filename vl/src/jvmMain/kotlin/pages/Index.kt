package pages

import kotlinx.html.*

fun HTML.index() {
    head {
        title("VL Game")
        link(
            rel = "stylesheet",
            type = "text/css",
            href = "static/common.css"
        )
    }
    body {
        attributes["bgcolor"] = "avlhfz"
        div {
            id = "root"
        }
        noScript {
            +"This application requires javascript to run. Its a React application, it *really* wont run well without it."
        }
        script(src = "/static/output.js") {}
    }
}

