package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.playerNotFound(id : Int) {
    head {
        title("Player with id $id not found")
    }
    body {
        +"404 - And this one is weird. The token was valid, but the player id associated with it doesn't!"
    }
}
