package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.gameIsOver(id : Int) {
    head {
        title("Game with id $id is over")
    }
    body {
        +"404"
    }
}
