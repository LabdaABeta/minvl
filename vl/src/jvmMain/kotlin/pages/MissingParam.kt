package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.missingParam(param : String) {
    head {
        title("Bad request, missing $param parameter")
    }
    body {
        +"5xx"
    }
}

