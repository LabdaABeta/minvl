package pages

import com.google.gson.JsonParseException
import kotlinx.html.*
import kotlin.reflect.KType

fun HTML.badParam(name : String, value : String, type : KType, exception : JsonParseException) {
    head {
        title("Bad parameter $name")
    }
    body {
        h1 { +name }
        h3 { +"Value Passed" }
        pre { +value }
        h3 { +"Type requested" }
        pre { +type.toString() }
        h3 { +"Exception" }
        pre { +exception.toString() }
        pre { +exception.stackTraceToString() }
    }
}