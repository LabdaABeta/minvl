package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.badVLAction(action : String) {
    head {
        title("The action string is invalid")
    }
    body {
        +"The invalid string was: $action"
    }
}

fun HTML.badVLBoard(board : String) {
    head {
        title("The board string is invalid")
    }
    body {
        +"The invalid string was: $board"
    }
}