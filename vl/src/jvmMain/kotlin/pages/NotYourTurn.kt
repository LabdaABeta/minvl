package pages

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.notYourTurn(id : Int) {
    head {
        title("It is not your turn")
    }
    body {
        +"404"
    }
}
