package pages

import io.ktor.http.*
import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

fun HTML.unauthorized(name : String) {
    head {
        title("Token expired or Bad password")
    }
    body {
        +"401 - $name no good"
    }
}

fun unauthorizedHtmlResponse(name : String) = CRUDResponse.HtmlResponse(HttpStatusCode.Unauthorized) { unauthorized(name) }