import data.objects.Game
import data.objects.Player
import data.tables.Games
import data.tables.Players
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import pages.index
import java.io.File
import java.sql.Connection

fun main() {
    // Environment variable access
    val port = System.getenv("VL_SERVER_PORT")?.toIntOrNull() ?: 8080
    val host = System.getenv("VL_SERVER_HOST") ?: "127.0.0.1"

    // Database setup
    val pwd = System.getProperty("user.dir")
    Database.connect("jdbc:sqlite:$pwd/db.sqlite", "org.sqlite.JDBC")
    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
    transaction { SchemaUtils.createMissingTablesAndColumns(Players, Games) }

    // Server behaviour
    embeddedServer(Netty, port = port, host = host) {
        // Installation
        install(ContentNegotiation) {
            gson {  }
        }

        install(CORS) {
            method(HttpMethod.Delete)
            method(HttpMethod.Put)
            method(HttpMethod.Post)
            method(HttpMethod.Get)
            anyHost()
        }

        routing {
            // API
            Game.route(this)
            Player.route(this)

            // UI
            get("/") {
                call.respondHtml(HttpStatusCode.OK) { index() }
            }

            // Static
            static("/static") {
                resources()
            }
            get("/favicon.ico") {
                val piece = listOf("vampire", "leprechaun", "human", "fairy", "zombie").random()
                val colour = listOf("light", "dark").random()
                val iconURL = Authentication::class.java.getResource("sprites/${piece}_${colour}.ico")
                val iconFile = File(iconURL.toURI())
                call.respondFile(iconFile)
            }
        }
    }.start(wait = true)
}