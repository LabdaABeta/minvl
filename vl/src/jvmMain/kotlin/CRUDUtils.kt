import com.google.gson.Gson
import com.google.gson.JsonParseException
import io.ktor.application.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.pipeline.*
import kotlinx.html.HTML
import pages.badParam
import pages.missingParam
import kotlin.reflect.KCallable
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.callSuspendBy
import kotlin.reflect.full.createType
import kotlin.reflect.full.withNullability
import kotlin.reflect.jvm.javaType

sealed class CRUDResponse {
    data class HtmlResponse(val status : HttpStatusCode, val fill : HTML.() -> Unit) : CRUDResponse()
    data class DataResponse(val status : HttpStatusCode, val data : Any) : CRUDResponse()
}

fun Any.asCRUDResponse(status : HttpStatusCode = HttpStatusCode.OK) = CRUDResponse.DataResponse(status, this)

class DelayedParameters(val load : suspend () -> Parameters) {
    private var params : Parameters? = null

    suspend fun get(key : String) : String? {
        if (params == null)
            params = load()

        return params?.get(key)
    }
}

suspend fun PipelineContext<Unit, ApplicationCall>.doCRUD(responder : KCallable<CRUDResponse>) {
    val rawParams = call.parameters
    val urlParams = call.request.queryParameters
    val params = DelayedParameters { call.receiveParameters() }
    val args = mutableMapOf<KParameter, Any?>()

    for (parameter in responder.parameters) {
        val name = parameter.name ?: "value"
        val value = rawParams[name] ?: urlParams[name] ?: params.get(name)

        if (value == null && !parameter.isOptional) {
            call.respondHtml(HttpStatusCode.BadRequest) { missingParam(parameter.name ?: "value") }
            return
        }

        if (value != null) {
            if (parameter.type == String::class.createType().withNullability(true)) {
                args[parameter] = value
            } else {
                try {
                    val argData : Any? = Gson().fromJson(value, parameter.type.javaType)
                    args[parameter] = argData
                } catch (e : JsonParseException) {
                    call.respondHtml(HttpStatusCode.BadRequest) { badParam(parameter.name ?: "value", value, parameter.type, e) }
                    return
                }
            }
        }
    }

    // TODO: double check that callSuspendBy is always safe
    when (val result = responder.callSuspendBy(args)) {
        is CRUDResponse.HtmlResponse -> call.respondHtml(result.status, result.fill)
        is CRUDResponse.DataResponse -> call.respond(result.status, result.data)
    }
}

// Routes a post request
fun Routing.routeCreate(endpoint : String, responder : KCallable<CRUDResponse>) {
    post(endpoint) {
        doCRUD(responder)
    }
}

// Routes a get request
fun Routing.routeRead(endpoint : String, responder : KCallable<CRUDResponse>) {
    get(endpoint) {
        doCRUD(responder)
    }
}

// Routes a put request
fun Routing.routeUpdate(endpoint : String, responder : KCallable<CRUDResponse>) {
    put(endpoint) {
        doCRUD(responder)
    }
}

// Routes a delete request
fun Routing.routeDelete(endpoint : String, responder : KCallable<CRUDResponse>) {
    delete(endpoint) {
        doCRUD(responder)
    }
}