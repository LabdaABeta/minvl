import com.github.salomonbrys.gradle.sass.SassExtension
import com.github.salomonbrys.gradle.sass.SassTask
import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack
import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpackConfig.Mode

plugins {
    kotlin("multiplatform") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"
    id("com.github.salomonbrys.gradle.sass") version "1.2.0"
    application
}
group = "net.labprogramming"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/kotlin/ktor")
    }
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlinx")
    }
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlin-js-wrappers")
    }
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        withJava()
    }
    js {
        browser {
            binaries.executable()
            webpackTask {
                if (System.getenv("VL_DEBUG_MODE") == "true")
                    mode = Mode.DEVELOPMENT
                cssSupport.enabled = true
                sourceMaps = true
            }
            runTask {
                if (System.getenv("VL_DEBUG_MODE") == "true")
                    mode = Mode.DEVELOPMENT
                cssSupport.enabled = true
                sourceMaps = true
            }
        }
    }
    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("io.ktor:ktor-server-netty:1.4.0")
                implementation("io.ktor:ktor-html-builder:1.4.0")
                implementation("io.ktor:ktor-gson:1.4.0")
                implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.2")
                implementation("org.jetbrains.exposed:exposed-core:0.27.1")
                implementation("org.jetbrains.exposed:exposed-dao:0.27.1")
                implementation("org.jetbrains.exposed:exposed-jdbc:0.27.1")
                implementation("org.xerial:sqlite-jdbc:3.21.0.1")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.0.0-RC2")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.0-RC2")
                implementation("ch.qos.logback:logback-classic:1.2.3")
                implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.10")
                implementation("com.google.code.gson:gson:2.8.6")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
        val jsMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.7.2")
                implementation("org.jetbrains:kotlin-react:16.13.1-pre.110-kotlin-1.4.10")
                implementation("org.jetbrains:kotlin-react-dom:16.13.1-pre.110-kotlin-1.4.10")
                implementation("org.jetbrains:kotlin-styled:1.0.0-pre.110-kotlin-1.4.10")
            }
        }
        val jsTest by getting
    }
}
application {
    mainClassName = "ServerKt"
}
configure<SassExtension> {
    download {
        version = "1.26.5"
    }
}
tasks.named<SassTask>("sassCompile") {
    source = fileTree("src/jvmMain/resources/sass")
    outputDir = file("$buildDir/processedResources/jvm/main")
    noSourceMap()
    embedSourceMap { embedSource = false }
}
tasks.getByName<KotlinWebpack>("jsBrowserProductionWebpack") {
    outputFileName = "output.js"
    sourceMaps = true
}
tasks.getByName<Jar>("jvmJar") {
    dependsOn(tasks.getByName("jsBrowserProductionWebpack"))
    dependsOn(":sassCompile")
    val jsBrowserProductionWebpack = tasks.getByName<KotlinWebpack>("jsBrowserProductionWebpack")
    from(File(jsBrowserProductionWebpack.destinationDirectory, jsBrowserProductionWebpack.outputFileName))
    from(File(jsBrowserProductionWebpack.destinationDirectory, jsBrowserProductionWebpack.outputFileName + ".map"))
}
tasks.getByName<JavaExec>("run") {
    dependsOn(tasks.getByName<Jar>("jvmJar"))
    classpath(tasks.getByName<Jar>("jvmJar"))
}