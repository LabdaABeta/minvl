fun r(s:String):String=s.chunked(16){it.chunked(2){c->"${27.toChar()}[4"+when(c[1]){in "VLHFZA"->0;in "vlhfza"->7;else->3}+"m${27.toChar()}[3"+when(c[1]){in "vV"->1;in "lL"->2;in "hH"->4;in "fFaA"->5;in "zZ"->6;else->0}+"m"+if(c[1]=='.')"×" else if(c[1]in "aA")" ➊➋➌➍➎➏"[c[0]-'0']else " ①②③④⑤⑥⑦⑧⑨⑩"[c[0]-'0']}.joinToString("")}.joinToString("${27.toChar()}[m\n")+"${27.toChar()}[m\n"

fun String.toggleCase() = map {
    when {
        it.isUpperCase() -> it.toLowerCase()
        it.isLowerCase() -> it.toUpperCase()
        else -> it
    }
}.joinToString("")

fun Char.int() = "$this".toInt()

fun VL(s : String? = null, a : String? = null) : String =
    if (s == null)
        "2V1Z2V3F3F2V1Z2V2L2L2H2H2H2H2L2L" + "0_".repeat(32) + "2l2l2h2h2h2h2l2l2v1z2v3f3f2v1z2v"
    else if (a == null)
        (0..7).flatMap { x ->
            (0..7).flatMap { y ->
                ("uUnNrReEdDsSlLwW").flatMap { d ->
                    (0..7).map { l ->
                        "$x$y$d$l"
                    }
                }
            }
        }.filter { VL(s, it) != "" }.distinct().joinToString(",")
    else {
        val x = a[0].int()
        val y = a[1].int()
        val direction = a[2].toUpperCase()
        val split = a[2].isUpperCase()
        val count = a[3].int()

        val dx = when (direction) {
            in "NRE" -> 1
            in "SLW" -> -1
            else -> 0
        }

        val dy = when (direction) {
            in "UNW" -> -1
            in "EDS" -> 1
            else -> 0
        }

        val sourceIdx = 16 * y + 2 * x
        val targetIdx = 16 * (y + dy) + 2 * (x + dx)

        if (x !in 0..7 || y !in 0..7 || (x + dx) !in 0..7 || (y + dy) !in 0..7)
            ""
        else {
            val sourceUnit = s[sourceIdx + 1]
            val sourceSize = s[sourceIdx].int()
            val unit = if (sourceUnit == 'a') 'f' else sourceUnit
            val floor = if (sourceUnit == 'a') '.' else '_'
            val source = sourceIdx .. sourceIdx + 1
            val target = targetIdx .. targetIdx + 1
            val targetUnit = s[targetIdx + 1]
            val targetSize = s[targetIdx].int()

            if (sourceUnit !in "vlhfza")
                ""
            else {
                if (split) {
                    if (sourceSize <= count || count == 0) "" else {
                        val emptied = if (sourceSize == count)
                            s.replaceRange(source, "0$floor")
                        else
                            s.replaceRange(sourceIdx .. sourceIdx, (sourceSize - count).toString())

                        when (targetUnit) {
                            '_' -> emptied.replaceRange(target, "$count$unit").toggleCase()
                            '.' -> if (sourceUnit !in "fa") "" else emptied.replaceRange(target, "${count}a").toggleCase()
                            else -> ""
                        }
                    }
                } else {
                    when (count) {
                        0 ->
                            when (sourceUnit) {
                                'l' ->
                                    if (targetUnit == '.')
                                        s.replaceRange(target, "0_").toggleCase()
                                    else ""

                                'h' ->
                                    if (targetUnit == '_')
                                        s.replaceRange(target, "0.").toggleCase()
                                    else ""

                                else -> ""
                            }
                        1 ->
                            when (targetUnit) {
                                '.' ->
                                    if (sourceUnit in "fa")
                                        s.replaceRange(target, "${sourceSize}a")
                                         .replaceRange(source, "0$floor")
                                         .toggleCase()
                                    else
                                        ""

                                '_' ->
                                    s.replaceRange(target, "$sourceSize$unit")
                                        .replaceRange(source, "0$floor")
                                        .toggleCase()

                                in "vlhfz" -> {
                                    if (unit == targetUnit)
                                        s.replaceRange(target, "${sourceSize + targetSize}$unit")
                                         .replaceRange(source, "0$floor").toggleCase()
                                    else
                                        ""
                                }

                                'a' -> {
                                    if (unit == 'f')
                                        s.replaceRange(target, "${sourceSize + targetSize}a")
                                         .replaceRange(source, "0$floor").toggleCase()
                                    else
                                        ""
                                }

                                'V' -> {
                                    val size = when (sourceUnit) {
                                        in "hz" -> sourceSize - (targetSize * 2)
                                        in "lfa" -> sourceSize - (targetSize / 2)
                                        else -> sourceSize - targetSize
                                    }

                                    if (size < 0)
                                        ""
                                    else if (size == 0)
                                        s.replaceRange(target, "0_")
                                                .replaceRange(source, "0$floor")
                                                .toggleCase()
                                    else
                                        s.replaceRange(target, "$size$unit")
                                            .replaceRange(source, "0$floor")
                                            .toggleCase()
                                }

                                'L' -> {
                                    val size = when (sourceUnit) {
                                        in "vz" -> sourceSize - (targetSize * 2)
                                        in "hfa" -> sourceSize - (targetSize / 2)
                                        else -> sourceSize - targetSize
                                    }

                                    if (size < 0)
                                        ""
                                    else if (size == 0)
                                        s.replaceRange(target, "0_")
                                                .replaceRange(source, "0$floor")
                                                .toggleCase()
                                    else
                                        s.replaceRange(target, "$size$unit")
                                            .replaceRange(source, "0$floor")
                                            .toggleCase()
                                }

                                'H' -> {
                                    val size = when (sourceUnit) {
                                        in "lfa" -> sourceSize - (targetSize * 2)
                                        in "vz" -> sourceSize - (targetSize / 2)
                                        else -> sourceSize - targetSize
                                    }

                                    if (size < 0)
                                        ""
                                    else if (size == 0)
                                        s.replaceRange(target, "0_")
                                                .replaceRange(source, "0$floor")
                                                .toggleCase()
                                    else
                                        (if (sourceUnit == 'z') s else s.replaceRange(source, "0$floor"))
                                            .replaceRange(target, "$size$unit")
                                            .toggleCase()
                                }

                                'F' -> {
                                    val size = when (sourceUnit) {
                                        in "hz" -> sourceSize - (targetSize * 2)
                                        in "vl" -> sourceSize - (targetSize / 2)
                                        else -> sourceSize - targetSize
                                    }

                                    if (size < 0)
                                        ""
                                    else if (size == 0)
                                        s.replaceRange(target, "0_")
                                                .replaceRange(source, "0$floor")
                                                .toggleCase()
                                    else
                                        s.replaceRange(target, "$size$unit")
                                            .replaceRange(source, "0$floor")
                                            .toggleCase()
                                }

                                'A' -> {
                                    if (sourceUnit in "fa") {
                                        val size = sourceSize - targetSize
                                        if (size < 0)
                                            ""
                                        else if (size == 0)
                                            s.replaceRange(target, "0_")
                                                    .replaceRange(source, "0$floor")
                                                    .toggleCase()
                                        else
                                            s.replaceRange(target, "${size}a")
                                                .replaceRange(source, "0$floor")
                                                .toggleCase()
                                    } else ""
                                }

                                'Z' -> {
                                    val size = when (sourceUnit) {
                                        in "hfa" -> sourceSize - (targetSize * 2)
                                        in "vl" -> sourceSize - (targetSize / 2)
                                        else -> sourceSize - targetSize
                                    }

                                    if (size < 0)
                                        ""
                                    else if (size == 0)
                                        s.replaceRange(target, "0_")
                                                .replaceRange(source, "0$floor")
                                                .toggleCase()
                                    else
                                        s.replaceRange(target, "$size$unit")
                                            .replaceRange(source, "0$floor")
                                            .toggleCase()
                                }

                                else -> ""
                            }
                        else -> if (count > sourceSize) "" else {
                            when (targetUnit) {
                                '.' ->
                                    if (sourceUnit in "fa")
                                        VL(
                                            s.replaceRange(target, "${sourceSize}a")
                                             .replaceRange(source, "0$floor"), "${x+dx}${y+dy}${a[2]}${count - 1}"
                                        )
                                    else
                                        ""

                                '_' ->
                                    VL(
                                        s.replaceRange(target, "$sourceSize$unit")
                                            .replaceRange(source, "0$floor"), "${x+dx}${y+dy}${a[2]}${count - 1}"
                                    )

                                else ->
                                    ""
                            }
                        }
                    }
                }
            }
        }
    }
